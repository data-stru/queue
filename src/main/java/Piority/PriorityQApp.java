/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Piority;

import java.io.IOException;

/**
 *
 * @author informatics
 */
class PriorityQApp
{
public static void main(String[] args) throws IOException
{
PriorityQ thePQ = new PriorityQ(5);
thePQ.insert(100);
thePQ.insert(5);
thePQ.insert(20);
thePQ.insert(10);
thePQ.insert(100);
while( !thePQ.isEmpty() )
{
long item = thePQ.remove();
System.out.print(item + " "); // 10, 20, 30, 40, 50
} // end while
System.out.println(" ");
} // end main()
//-------------------------------------------------------------
} // end class PriorityQApp
